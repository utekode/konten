---
judul: "Facebook"
draft: true
---

+ [Social Plugins](https://developers.facebook.com/docs/plugins/)
+ [Page Plugin](https://developers.facebook.com/docs/plugins/page-plugin/)

### Embedded Posts
+ [Embedded Posts](https://developers.facebook.com/docs/plugins/embedded-posts/ "Facebook Social Plugin Embedded Posts")

`https://www.facebook.com/20531316728/posts/10154009990506729/`

```html
<script async defer src="https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v3.2"></script>  
<div class="fb-post" 
    data-href="https://www.facebook.com/20531316728/posts/10154009990506729/"
    data-width="500"></div>
```

`![fb-post](https://www.facebook.com/20531316728/posts/10154009990506729/ "read")`

![fb-post](https://www.facebook.com/20531316728/posts/10154009990506729/ "read")

### Embedded Video & Live Video Player
+ [Embedded Video & Live Video Player](https://developers.facebook.com/docs/plugins/embedded-video-player/ "Embedded Video")

`https://www.facebook.com/facebook/videos/10153231379946729/`

```html
<!-- Load Facebook SDK for JavaScript -->
<div id="fb-root"></div>
<script async defer src="https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v3.2"></script>

<!-- Your embedded video player code -->
<div class="fb-video" data-href="https://www.facebook.com/facebook/videos/10153231379946729/" data-width="500" data-show-text="false">
  <div class="fb-xfbml-parse-ignore">
    <blockquote cite="https://www.facebook.com/facebook/videos/10153231379946729/">
    <a href="https://www.facebook.com/facebook/videos/10153231379946729/">How to Share With Just Friends</a>
    <p>How to share with just friends.</p>
    Posted by <a href="https://www.facebook.com/facebook/">Facebook</a> on Friday, December 5, 2014
    </blockquote>
  </div>
</div>
```

`![fb-video](https://www.facebook.com/facebookapp/videos/10153231379946729/ "watch")`

![fb-video](https://www.facebook.com/facebookapp/videos/10153231379946729/ "watch")