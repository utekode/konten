## Google Play Store 
+ `https://play.google.com/store/apps/details?id=org.telegram.messenger`

Apps
+ Web: `http://play.google.com/store/apps/details?id=`
+ App: `market://details?id=`

```
<a href='https://play.google.com/store/apps/details?id=org.thunderdog.challegram&pcampaignid=pcampaignidMKT-Other-global-all-co-prtnr-py-PartBadge-Mar2515-1'><img alt='Temukan di Google Play' src='https://play.google.com/intl/id/badges/static/images/badges/id_badge_web_generic.png'/></a>
```

Show all apps by a specific publisher
+ Web: `http://play.google.com/store/search?q=pub:`
+ App: `market://search?q=pub:`

Search for apps that using the Query on its title or description
+ Web: `http://play.google.com/store/search?q=`
+ App: `market://search?q=`