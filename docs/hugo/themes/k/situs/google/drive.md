---
judul: "Google Drive"
draft: true
---

### Folder {.x}
+ `https://drive.google.com/drive/folders/1yQ174zF14ZdaJUU5ZhRbaycX-UyPSgkS?usp=sharing`
+ Folder with G Suite/Google Apps domain `https://drive.google.com/a/MY.DOMAIN.COM/embeddedfolderview?id=FOLDER-ID#grid`

`![Folder](https://drive.google.com/drive/folders/1yQ174zF14ZdaJUU5ZhRbaycX-UyPSgkS?usp=sharing "folder")`

![Folder](https://drive.google.com/drive/folders/1yQ174zF14ZdaJUU5ZhRbaycX-UyPSgkS?usp=sharing "folder")

#### daftar {.x}
+ Embed list `https://drive.google.com/embeddedfolderview?id=FOLDER-ID#list`

`![Sematan Drive tampil daftar](https://drive.google.com/embeddedfolderview?id=1yQ174zF14ZdaJUU5ZhRbaycX-UyPSgkS#list "list")`

![Sematan Drive tampil daftar](https://drive.google.com/embeddedfolderview?id=1yQ174zF14ZdaJUU5ZhRbaycX-UyPSgkS#list "list")

{{< x >}}
#### ubin atau keluku {.x}
+ Embed grid `https://drive.google.com/embeddedfolderview?id=FOLDER-ID#grid`

`![Sematan Drive tampil keluku](https://drive.google.com/embeddedfolderview?id=1yQ174zF14ZdaJUU5ZhRbaycX-UyPSgkS#grid "grid")`

![Sematan Drive tampil keluku](https://drive.google.com/embeddedfolderview?id=1yQ174zF14ZdaJUU5ZhRbaycX-UyPSgkS#grid "grid")

{{< x >}}
{{< x >}}
### File {.x}

+ File viewer `https://drive.google.com/file/d/1vI4sQyGSAesDZ0KmTn1ceT7VojsDzRCk/view`

{{< x >}}

[Copy and Make any shared Google Drive File your own. Amit Agarwal @ labnol.org](https://www.labnol.org/internet/direct-links-for-google-drive/28356/)  

### Image
+ Preview `https://drive.google.com/file/d/1vI4sQyGSAesDZ0KmTn1ceT7VojsDzRCk/preview`
+ Direct `https://drive.google.com/uc?export=view&id=1vI4sQyGSAesDZ0KmTn1ceT7VojsDzRCk`