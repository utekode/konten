---
judul: "Situs Populer"
draft: true
---

{{< t "docs/hugo/themes/k/markah/render-link" >}}
{{< t "docs/hugo/themes/k/markah/render-image" >}}
{{< t "docs/hugo/themes/k/markah/render-heading" >}}

+ [disqus.com](https://disqus.com)
+ [fb.me](https://fb.me/)
+ [facebook.com](https://facebook.com)
+ [github.com](https://github.com)
+ [gitlab.com](https://gitlab.com)
+ [google.com](https://google.com)
+ [google.com/maps](https://www.google.com/maps/)
+ [goo.gl/maps/Vt3u8NSi19f8ZEKaA](https://goo.gl/maps/Vt3u8NSi19f8ZEKaA)
+ [drive.google.com](https://drive.google.com/)
  + [drive.google.com/drive/folders](https://drive.google.com/drive/folders/)
  + [docs.google.com/document](https://docs.google.com/document/)
  + [docs.google.com/spreadsheets](https://docs.google.com/spreadsheets/)
  + [docs.google.com/presentation](https://docs.google.com/presentation/)
  + [docs.google.com/forms](https://docs.google.com/forms/)
  + [docs.google.com/drawings](https://docs.google.com/drawings/)
+ [play.google.com](https://play.google.com)
  + [Telegram Messenger](https://play.google.com/store/apps/details?id=org.telegram.messenger)
  + [play.google.com/store/books](https://play.google.com/store/books)
  + [play.google.com/store/movies](https://play.google.com/store/movies)
+ [gohugo.io](https://gohugo.io)
+ [instagr.am](https://instagr.am/)
+ [instagram.com](https://instagram.com)
+ [line.me](https://line.me)
+ [joinmastodon.org](https://joinmastodon.org/)
+ [openstreetmap.org](https://openstreetmap.org/)
+ [pinterest.com](https://pinterest.com)

## Pinterest {.x}
https://www.pinterest.com/pinterest/
+ [Widget builder](https://developers.pinterest.com/tools/widget-builder/)
### Pin
`https://www.pinterest.com/pin/99360735500167749/`
### Board
`https://www.pinterest.com/pinterest/official-news/`
### Profile
`https://www.pinterest.com/pinterest/`

{{< x >}}

+ [sketchfab.com](https://sketchfab.com)

## Sketchfab {.x}
```html
<div class="sketchfab-embed-wrapper">
    <iframe title="A 3D model" width="640" height="480" src="https://sketchfab.com/models/442c548d94744641ba279ae94b5f45ec/embed?autospin=0.2&amp;autostart=1&amp;preload=1&amp;ui_controls=0&amp;ui_infos=0&amp;ui_inspector=0&amp;ui_stop=0&amp;ui_watermark=0&amp;ui_watermark_link=0" frameborder="0" allow="autoplay; fullscreen; vr" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>
    <p style="font-size: 13px; font-weight: normal; margin: 5px; color: #4A4A4A;">
        <a href="https://sketchfab.com/3d-models/cube-test-442c548d94744641ba279ae94b5f45ec?utm_medium=embed&utm_source=website&utm_campaign=share-popup" target="_blank" style="font-weight: bold; color: #1CAAD9;">Cube Test</a>
        by <a href="https://sketchfab.com/james?utm_medium=embed&utm_source=website&utm_campaign=share-popup" target="_blank" style="font-weight: bold; color: #1CAAD9;">James</a>
        on <a href="https://sketchfab.com?utm_medium=embed&utm_source=website&utm_campaign=share-popup" target="_blank" style="font-weight: bold; color: #1CAAD9;">Sketchfab</a>
    </p>
</div>
```
{{< x >}}

+ [soundcloud.com](https://soundcloud.com)

## SoundCloud {.x}
+ [Use SoundCloud to share sound all over the web](https://soundcloud.com/pages/embed)

### Profile

```html
<iframe allowtransparency="true" scrolling="no" frameborder="no" src="https://w.soundcloud.com/icon/?url=http%3A%2F%2Fsoundcloud.com%2Fundefined&color=orange_white&size=32" style="width: 32px; height: 32px;"></iframe>
```

### Player
```html
<iframe width="100%" height="166" scrolling="no" frameborder="no" allow="autoplay" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/34019569&color=0066cc"></iframe><div style="font-size: 10px; color: #cccccc;line-break: anywhere;word-break: normal;overflow: hidden;white-space: nowrap;text-overflow: ellipsis; font-family: Interstate,Lucida Grande,Lucida Sans Unicode,Lucida Sans,Garuda,Verdana,Tahoma,sans-serif;font-weight: 100;"><a href="https://soundcloud.com/the-bugle" title="The Bugle" target="_blank" style="color: #cccccc; text-decoration: none;">The Bugle</a> · <a href="https://soundcloud.com/the-bugle/bugle-179-playas-gon-play" title="Bugle 179 - Playas gon play" target="_blank" style="color: #cccccc; text-decoration: none;">Bugle 179 - Playas gon play</a></div>
```

### Playlist / Sets
`https://soundcloud.com/katxuisadorable/sets/lofi-fun`
```html
<iframe width="100%" height="450" scrolling="no" frameborder="no" allow="autoplay" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/playlists/360817706&color=%23ff5500&auto_play=false&hide_related=false&show_comments=true&show_user=true&show_reposts=false&show_teaser=true"></iframe><div style="font-size: 10px; color: #cccccc;line-break: anywhere;word-break: normal;overflow: hidden;white-space: nowrap;text-overflow: ellipsis; font-family: Interstate,Lucida Grande,Lucida Sans Unicode,Lucida Sans,Garuda,Verdana,Tahoma,sans-serif;font-weight: 100;"><a href="https://soundcloud.com/katxuisadorable" title="Katxu Is Adorable" target="_blank" style="color: #cccccc; text-decoration: none;">Katxu Is Adorable</a> · <a href="https://soundcloud.com/katxuisadorable/sets/lofi-fun" title="Lofi fun" target="_blank" style="color: #cccccc; text-decoration: none;">Lofi fun</a></div>
```
{{< x >}}

+ [open.spotify.com](https://open.spotify.com)

## Spotify {.x}
+ [Embed](https://developer.spotify.com/documentation/widgets/generate/embed/)

### Track 
`https://open.spotify.com/track/2RycgtfFQZOPgEGrzBGE0j`

```html
<iframe src="https://open.spotify.com/embed/track/2RycgtfFQZOPgEGrzBGE0j" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>
```

### Album 
`https://open.spotify.com/album/4RuzGKLG99XctuBMBkFFOC`

```html
<iframe src="https://open.spotify.com/embed/album/1DFixLWuPkv3KT3TnV35m3" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>
```
{{< x >}}

+ [t.me](https://t.me/)
+ [telegram.org](https://telegram.org)

## Telegram {.x}
+ [Widgets](https://core.telegram.org/widgets)

### Post
+ [Post Widget](https://core.telegram.org/widgets/posts)

`https://t.me/durov/43`

```html
<script async src="https://telegram.org/js/telegram-widget.js?14" data-telegram-post="telegram/83" data-width="100%"></script>
```

`https://t.me/telegram/83`

```html
<script async src="https://telegram.org/js/telegram-widget.js?14" data-telegram-post="telegram/83" data-width="100%"></script>
```

### comments.app
+ [comments.app](https://comments.app/)
+ [@discussbot](https://t.me/discussbot)

{{< x >}}

+ [t.co](https://t.co/)
+ [twitter.com](https://twitter.com)
+ Mencoba logo [:logo:](twitter) Twitter via link hook
+ Mencoba logo {{< logo twitter >}} Twitter via shortcodes
+ [unsplash.com/collections/10748755/naturemin](https://unsplash.com/collections/10748755/naturemin)

## Unsplash {.x}
+ Photo `https://unsplash.com/photos/G42AHLa7CwM`
+ User `https://unsplash.com/@adjmotion`
+ Collection `https://unsplash.com/collections/60074628/stillh`
### Source 
[https://source.unsplash.com/collection/60074628/350x350](https://source.unsplash.com/collection/60074628/350x350)

{{< x >}}

+ [vercel.com](https://vercel.com)  
+ [vimeo.com](https://vimeo.com)

## Vimeo {.x}

+ [Embedding videos overview](https://vimeo.zendesk.com/hc/en-us/articles/224969968-Embedding-videos-overview)
+ [How to embed your gorgeous videos across the nets](https://vimeo.com/blog/post/how-to-embed-your-gorgeous-videos-across-the-nets/)
+ [Working with oEmbed: Embedding Videos](https://developer.vimeo.com/api/oembed/videos)

Vimeo
  
+ A regular Vimeo video `https://vimeo.com/{video_id}`
+ In a a showcase `https://vimeo.com/album/{album_id}/video/{video_id}`
+ On a channel `https://vimeo.com/channels/{channel_id}/{video_id}`
+ In a group `https://vimeo.com/groups/{group_id}/videos/{video_id}`
+ An On Demand video `https://vimeo.com/ondemand/{ondemand_id}/{video_id}`

```html
<iframe title="vimeo-player" src="https://player.vimeo.com/video/336812660" width="640" height="360" frameborder="0" allowfullscreen></iframe>
```

{{< x >}}

+ [wa.me](https://wa.me/)
+ [whatsapp.com](https://whatsapp.com/)

## WhatsApp {.x}
+ [Chat URL](https://faq.whatsapp.com/general/chats/how-to-use-click-to-chat/?lang=en)
+ [wa.me](https://wa.me/)

{{< x >}}

+ [wikipedia.org](https://wikipedia.org/)
+ [youtu.be](https://youtu.be/)
+ [youtube.com](https://youtube.com)
+ [music.youtube.com](https://music.youtube.com)
