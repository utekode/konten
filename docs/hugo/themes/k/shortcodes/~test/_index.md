---
judul: "Coba"
deskripsi: "Deskripsi menggunakan [tautan](.), *miring*, **tebal**, dan `kode`. Juga mencoba [label](!!)"
---

{{< test/parse-url "https://user:password@www.example.org:8443/foo%20bar/baz?huey=Hello%20World&duey=Goodbye#wibble%20wobble" >}}

{{< test/parse-url "https://drive.google.com/embeddedfolderview?id=1cVQBCaJRnQNnakBb9HGVRjhIi6_Oh9nm#list" >}}

{{< test/parse-url "/foo%20bar/baz#wibble%20wobble" >}}

{{< test/parse-url "../foo/bar/baz" >}}

{{< test/parse-url "?huey=Hello%20World&duey=Goodbye" >}}

{{< test/parse-url "?huey=Hello%20World&duey=Goodbye,Daa#wibble%20wobble" >}}

{{< test/query "?huey=Hello&duey=World" >}}

{{< test/parse-x "https://user:password@www.example.org:8443/foo%20bar/baz?huey=Hello%20World&duey=Goodbye#wibble%20wobble" >}}

{{< test/parse-x "https://drive.google.com/embeddedfolderview?id=1cVQBCaJRnQNnakBb9HGVRjhIi6_Oh9nm#list" >}}

{{< test/parse-x ":soroti?huey=Hello&duey=World" >}}

{{< test/parse-x "!penting?lang=id&id=username" >}}

{{< test/parse-x "?javanese" >}}