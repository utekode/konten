---
judul: "Ikon Vektor"
deskripsi: "SVG"
---

## Ikon

· 
: kumentor

· 
: atak

· 
: aksesibilitas

{{< ikon bagi >}}
: bagi  

{{< ikon kirim >}}
: kirim  

{{< ikon susur >}}
: susur  

{{< ikon terang >}}
: terang  

{{< ikon gelap >}}
: gelap 

{{< ikon folder >}}
: folder  

{{< ikon folder_o >}}
: folder_o 

{{< ikon avatar >}}
: avatar  

{{< ikon avatar_o >}}
: avatar_o  

{{< ikon bahasa >}}
: bahasa  

{{< ikon teks_x >}}
: teks_x  

{{< ikon bars >}}
: bars  

{{< ikon android >}}
: android  

{{< ikon droid >}}
: droid  

{{< ikon laman >}}
: laman  

{{< ikon laman_o >}}
: laman_o  

{{< ikon audio >}}
: audio  

{{< ikon audio >}}
: audio  

{{< ikon awas >}}
: awas  

{{< ikon betul >}}
: betul  

{{< ikon bicara >}}
: bicara  

{{< ikon bintang >}}
: bintang  

{{< ikon bluetooth >}}
: bluetooth  

{{< ikon bookmark >}}
: bookmark  

{{< ikon kamera >}}
: kamera  

{{< ikon grafis >}}
: grafis  

{{< ikon grup >}}
: grup  

{{< ikon grup_o >}}
: grup_o  

{{< ikon info >}}
: info  

{{< ikon jam >}}
: jam  

{{< ikon suka >}}
: suka  

{{< ikon klip >}}
: klip  

{{< ikon taut >}}
: taut  

{{< ikon mata >}}
: mata  

{{< ikon mik >}}
: mik  

{{< ikon modif >}}
: modif  

{{< ikon obrol >}}
: obrol  

{{< ikon pak >}}
: pak  

· 
: pensil  

{{< ikon peta >}}
: peta  

{{< ikon repo >}}
: repo  

{{< ikon repo_o >}}
: repo_o  

{{< ikon rss >}}
: rss  

{{< ikon salah >}}
: salah  

{{< ikon salin >}}
: salin  

{{< ikon surel >}}
: surel  

{{< ikon tanya >}}
: tanya  

{{< ikon tempel >}}
: tempel  

{{< ikon teks >}}
: teks  

{{< ikon unduh >}}
: unduh  

{{< ikon unggah >}}
: unggah  

{{< ikon elipsis_v >}}
: elipsis_v  

{{< ikon video >}}
: video  

{{< ikon kam_video >}}
: kam_video  

{{< ikon nirkabel >}}
: nirkabel  

{{< ikon tote >}}
: tote  

{{< ikon film >}}
: film  

{{< ikon buku >}}
: buku  

{{< ikon buku_o >}}
: buku_o  

{{< ikon epub >}}
: epub  

{{< ikon app >}}
: app  

{{< ikon ekstensi >}}
: ekstensi  

{{< ikon foto >}}
: foto  

{{< ikon air >}}
: air  

{{< ikon api >}}
: api 

{{< ikon siklon >}}
: siklon 

{{< ikon giri >}}
: giri 


### brand logo
{{< logo youtube_c >}}
: youtube_c  

{{< logo gdocs >}}
: gdocs  

{{< logo gforms >}}
: gforms  

{{< logo gslides >}}
: gslides  

{{< logo gsheets >}}
: gsheets 

{{< logo gdrawings >}}
: gdrawings   

  
### brand & social icon
{{< logo disqus >}}
: disqus  

{{< logo facebook >}}
: facebook  

{{< logo github >}}
: github  

{{< logo gitlab >}}
: gitlab  

{{< logo google >}}
: google  

{{< logo hugo >}}
: hugo 

{{< logo instagram >}}
: instagram  

{{< logo line >}}
: line  

{{< logo mastodon >}}
: mastodon  

· 
: osm 

{{< logo pinterest >}}
: pinterest  

{{< logo sketchfab >}}
: sketchfab  

{{< logo soundcloud >}}
: soundcloud  

{{< logo spotify >}}
: spotify  

{{< logo telegram >}}
: telegram  

{{< logo twitter >}}
: twitter  

{{< logo unsplash >}}
: unsplash  

{{< logo vercel >}}
: vercel  

{{< logo vimeo >}}
: vimeo  

{{< logo wikipedia >}}
: wikipedia  

{{< logo whatsapp >}}
: whatsapp  

{{< logo youtube >}}
: youtube  

{{< logo yt_music >}}
: yt_music 

{{< logo internet_archive >}}
: internet-archive 

{{< logo gdrive >}} 
: gdrive 

