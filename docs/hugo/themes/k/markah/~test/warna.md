---
judul: Warna
---

<style>
  .tbl-bg tr span
  {
    background: inherit;
    background-clip: text;
    -webkit-background-clip: text;
    color: transparent;
    filter: invert(1) grayscale(1) contrast(9);
    -webkit-filter: invert(1) grayscale(1) contrast(9);
  }
  .tbl-cr tr span
  {
    color: white;
    mix-blend-mode: difference;
    filter: hue-rotate(180deg) contrast(9);
    -webkit-filter: hue-rotate(180deg) contrast(9);
  }
</style>

## Variabel Warna

<span style="font-weight:900;font-size:2em;color:hsla(0,0%, var(--li-fg),1)">VARIABEL WARNA</span>

## Middle Gray {.v}

<table class=tbl>
<caption>latar middle gray alpha</caption>
  <tr>
    <th>hslA</th>
    <th>A001</th>
    <th>A.92</th>
    <th>A.75</th>
    <th>A.50</th>
    <th>A.25</th>
    <th>A.24</th>
  </tr>
  <tr>
    <th>black</th>
    <td style="background:black;text-align: center;" colspan=6><span>black</span></td>
  </tr>
  <tr>
    <th>Geomean of 60:1</th>
    <td style="background:hsla(0, 0%, 40%, 1);"><span>A100</span></td>
    <td style="background:hsla(0, 0%, 40%, 0.92);"><span>A092</span></td>
    <td style="background:hsla(0, 0%, 40%, 0.75);"><span>A075</span></td>
    <td style="background:hsla(0, 0%, 40%, 0.50);"><span>A050</span></td>
    <td style="background:hsla(0, 0%, 40%, 0.25);"><span>A025</span></td>
    <td style="background:hsla(0, 0%, 40%, 0.24);"><span>A024</span></td>
  </tr>
  <tr>
    <th>CIELAB <code>L*a*b*</code></th>
    <td style="background:hsla(0, 0%, 47%, 1);"><span>A100</span></td>
    <td style="background:hsla(0, 0%, 47%, 0.92);"><span>A092</span></td>
    <td style="background:hsla(0, 0%, 47%, 0.75);"><span>A075</span></td>
    <td style="background:hsla(0, 0%, 47%, 0.50);"><span>A050</span></td>
    <td style="background:hsla(0, 0%, 47%, 0.25);"><span>A025</span></td>
    <td style="background:hsla(0, 0%, 47%, 0.24);"><span>A024</span></td>
  </tr>
  <tr>
    <th>18% gray card</th>
    <td style="background:hsla(0, 0%, 49%, 1);"><span>A100</span></td>
    <td style="background:hsla(0, 0%, 49%, 0.92);"><span>A092</span></td>
    <td style="background:hsla(0, 0%, 49%, 0.75);"><span>A075</span></td>
    <td style="background:hsla(0, 0%, 49%, 0.50);"><span>A005</span></td>
    <td style="background:hsla(0, 0%, 49%, 0.25);"><span>A025</span></td>
    <td style="background:hsla(0, 0%, 49%, 0.24);"><span>A024</span></td>
  </tr>
  <tr>
    <th>sRGB</th>
    <td style="background:hsla(0, 0%, 50%, 1);"><span>A100</span></td>
    <td style="background:hsla(0, 0%, 50%, 0.92);"><span>A092</span></td>
    <td style="background:hsla(0, 0%, 50%, 0.75);"><span>A075</span></td>
    <td style="background:hsla(0, 0%, 50%, 0.50);"><span>A050</span></td>
    <td style="background:hsla(0, 0%, 50%, 0.25);"><span>A025</span></td>
    <td style="background:hsla(0, 0%, 50%, 0.24);"><span>A024</span></td>
  </tr>
  <tr>
  <tr>
    <th>gray</th>
    <td style="background:gray;text-align: center;" colspan=6><span>gray</span></td>
  </tr>
    <th>Mac<br/>pre-OS X 10.6</th>
    <td style="background:hsla(0, 0%, 57%, 1);"><span>A100</span></td>
    <td style="background:hsla(0, 0%, 57%, 0.92);"><span>A092</span></td>
    <td style="background:hsla(0, 0%, 57%, 0.75);"><span>A075</span></td>
    <td style="background:hsla(0, 0%, 57%, 0.50);"><span>A050</span></td>
    <td style="background:hsla(0, 0%, 57%, 0.25);"><span>A025</span></td>
    <td style="background:hsla(0, 0%, 57%, 0.24);"><span>A024</span></td>
  </tr>
  <tr>
    <th>Absolute whiteness</th>
    <td style="background:hsla(0, 0%, 74%, 1);"><span>A100</span></td>
    <td style="background:hsla(0, 0%, 74%, 0.92);"><span>A092</span></td>
    <td style="background:hsla(0, 0%, 74%, 0.75);"><span>A075</span></td>
    <td style="background:hsla(0, 0%, 74%, 0.50);"><span>A050</span></td>
    <td style="background:hsla(0, 0%, 74%, 0.25);"><span>A025</span></td>
    <td style="background:hsla(0, 0%, 74%, 0.24);"><span>A024</span></td>
  </tr>
  <tr>
    <th>silver</th>
    <td style="background:silver;text-align: center;" colspan=6><span>silver</span></td>
  </tr>
  <tr>
    <th>white</th>
    <td style="background:white;text-align: center;" colspan=6><span>white</span></td>
  </tr>
</table>

<table class=tbl>
<caption>teks middle gray alpha</caption>
  <tr>
    <th>hslA</th>
    <th>A001</th>
    <th>A.92</th>
    <th>A.75</th>
    <th>A.50</th>
    <th>A.25</th>
    <th>A.24</th>
  </tr>
  <tr>
    <th>black</th>
    <td style="color:black;text-align: center;" colspan=6><span>black</span></td>
  </tr>
  <tr>
    <th>Geomean of 60:1</th>
    <td style="color:hsla(0, 0%, 40%, 1);"><span>A100</span></td>
    <td style="color:hsla(0, 0%, 40%, 0.92);"><span>A092</span></td>
    <td style="color:hsla(0, 0%, 40%, 0.75);"><span>A075</span></td>
    <td style="color:hsla(0, 0%, 40%, 0.50);"><span>A050</span></td>
    <td style="color:hsla(0, 0%, 40%, 0.25);"><span>A025</span></td>
    <td style="color:hsla(0, 0%, 40%, 0.24);"><span>A024</span></td>
  </tr>
  <tr>
    <th>CIELAB <code>L*a*b*</code></th>
    <td style="color:hsla(0, 0%, 47%, 1);"><span>A100</span></td>
    <td style="color:hsla(0, 0%, 47%, 0.92);"><span>A092</span></td>
    <td style="color:hsla(0, 0%, 47%, 0.75);"><span>A075</span></td>
    <td style="color:hsla(0, 0%, 47%, 0.50);"><span>A050</span></td>
    <td style="color:hsla(0, 0%, 47%, 0.25);"><span>A025</span></td>
    <td style="color:hsla(0, 0%, 47%, 0.24);"><span>A024</span></td>
  </tr>
  <tr>
    <th>18% gray card</th>
    <td style="color:hsla(0, 0%, 49%, 1);"><span>A100</span></td>
    <td style="color:hsla(0, 0%, 49%, 0.92);"><span>A092</span></td>
    <td style="color:hsla(0, 0%, 49%, 0.75);"><span>A075</span></td>
    <td style="color:hsla(0, 0%, 49%, 0.50);"><span>A005</span></td>
    <td style="color:hsla(0, 0%, 49%, 0.25);"><span>A025</span></td>
    <td style="color:hsla(0, 0%, 49%, 0.24);"><span>A024</span></td>
  </tr>
  <tr>
    <th>sRGB</th>
    <td style="color:hsla(0, 0%, 50%, 1);"><span>A100</span></td>
    <td style="color:hsla(0, 0%, 50%, 0.92);"><span>A092</span></td>
    <td style="color:hsla(0, 0%, 50%, 0.75);"><span>A075</span></td>
    <td style="color:hsla(0, 0%, 50%, 0.50);"><span>A050</span></td>
    <td style="color:hsla(0, 0%, 50%, 0.25);"><span>A025</span></td>
    <td style="color:hsla(0, 0%, 50%, 0.24);"><span>A024</span></td>
  </tr>
  <tr>
  <tr>
    <th>gray</th>
    <td style="color:gray;text-align: center;" colspan=6><span>gray</span></td>
  </tr>
    <th>Mac<br/>pre-OS X 10.6</th>
    <td style="color:hsla(0, 0%, 57%, 1);"><span>A100</span></td>
    <td style="color:hsla(0, 0%, 57%, 0.92);"><span>A092</span></td>
    <td style="color:hsla(0, 0%, 57%, 0.75);"><span>A075</span></td>
    <td style="color:hsla(0, 0%, 57%, 0.50);"><span>A050</span></td>
    <td style="color:hsla(0, 0%, 57%, 0.25);"><span>A025</span></td>
    <td style="color:hsla(0, 0%, 57%, 0.24);"><span>A024</span></td>
  </tr>
  <tr>
    <th>Absolute whiteness</th>
    <td style="color:hsla(0, 0%, 74%, 1);"><span>A100</span></td>
    <td style="color:hsla(0, 0%, 74%, 0.92);"><span>A092</span></td>
    <td style="color:hsla(0, 0%, 74%, 0.75);"><span>A075</span></td>
    <td style="color:hsla(0, 0%, 74%, 0.50);"><span>A050</span></td>
    <td style="color:hsla(0, 0%, 74%, 0.25);"><span>A025</span></td>
    <td style="color:hsla(0, 0%, 74%, 0.24);"><span>A024</span></td>
  </tr>
  <tr>
    <th>silver</th>
    <td style="color:silver;text-align: center;" colspan=6><span>silver</span></td>
  </tr>
  <tr>
    <th>white</th>
    <td style="color:white;text-align: center;" colspan=6><span>white</span></td>
  </tr>
</table>

<table class=tbl>
<caption>latar middle gray</caption>
  <tr>
    <th>hsLa</th>
    <th>≍A001</th>
    <th>≍A.92</th>
    <th>≍A.75</th>
    <th>≍A.50</th>
    <th>≍A.25</th>
    <th>≍A.24</th>
  </tr>
  <tr>
    <th>black</th>
    <td style="background:black;text-align: center;" colspan=6><span>black</span></td>
  </tr>
  <tr>
    <th>Geomean of 60:1</th>
    <td style="background:hsla(0, 0%, 40%, 1);"><span>L040%</span></td>
    <td style="background:hsla(0, 0%, 45%, 1);"><span>L045%</span></td>
    <td style="background:hsla(0, 0%, 55%, 1);"><span>L055%</span></td>
    <td style="background:hsla(0, 0%, 70%, 1);"><span>L070%</span></td>
    <td style="background:hsla(0, 0%, 85%, 1);"><span>L085%</span></td>
    <td style="background:hsla(0, 0%, 93%, 1);"><span>L093%</span></td>
  </tr>
  <tr>
    <th>CIELAB <code>L*a*b*</code></th>
    <td style="background:hsla(0, 0%, 47%, 1);"><span>L047%</span></td>
    <td style="background:hsla(0, 0%, 51%, 1);"><span>L051%</span></td>
    <td style="background:hsla(0, 0%, 60%, 1);"><span>L060%</span></td>
    <td style="background:hsla(0, 0%, 73%, 1);"><span>L073%</span></td>
    <td style="background:hsla(0, 0%, 87%, 1);"><span>L087%</span></td>
    <td style="background:hsla(0, 0%, 87%, 1);"><span>L087%</span></td>
  </tr>
  <tr>
    <th>18% gray card</th>
    <td style="background:hsla(0, 0%, 49%, 1);"><span>L049%</span></td>
    <td style="background:hsla(0, 0%, 53%, 1);"><span>L053%</span></td>
    <td style="background:hsla(0, 0%, 62%, 1);"><span>L062%</span></td>
    <td style="background:hsla(0, 0%, 74%, 1);"><span>L074%</span></td>
    <td style="background:hsla(0, 0%, 87%, 1);"><span>L087%</span></td>
    <td style="background:hsla(0, 0%, 88%, 1);"><span>L088%</span></td>
  </tr>
  <tr>
    <th>sRGB</th>
    <td style="background:hsla(0, 0%, 50%, 1);"><span>L050%</span></td>
    <td style="background:hsla(0, 0%, 54%, 1);"><span>L054%</span></td>
    <td style="background:hsla(0, 0%, 63%, 1);"><span>L063%</span></td>
    <td style="background:hsla(0, 0%, 75%, 1);"><span>L075%</span></td>
    <td style="background:hsla(0, 0%, 87%, 1);"><span>L087%</span></td>
    <td style="background:hsla(0, 0%, 88%, 1);"><span>L088%</span></td>
  </tr>
  <tr>
  <tr>
    <th>gray</th>
    <td style="background:gray;text-align: center;" colspan=6><span>gray</span></td>
  </tr>
    <th>Mac<br/>pre-OS X 10.6</th>
    <td style="background:hsla(0, 0%, 57%, 1);"><span>L057%</span></td>
    <td style="background:hsla(0, 0%, 60%, 1);"><span>L060%</span></td>
    <td style="background:hsla(0, 0%, 67%, 1);"><span>L067%</span></td>
    <td style="background:hsla(0, 0%, 78%, 1);"><span>L078%</span></td>
    <td style="background:hsla(0, 0%, 89%, 1);"><span>L089%</span></td>
    <td style="background:hsla(0, 0%, 90%, 1);"><span>L090%</span></td>
  </tr>
  <tr>
    <th>Absolute whiteness</th>
    <td style="background:hsla(0, 0%, 74%, 1);"><span>L074%</span></td>
    <td style="background:hsla(0, 0%, 76%, 1);"><span>L076%</span></td>
    <td style="background:hsla(0, 0%, 80%, 1);"><span>L080%</span></td>
    <td style="background:hsla(0, 0%, 87%, 1);"><span>L087%</span></td>
    <td style="background:hsla(0, 0%, 93%, 1);"><span>L093%</span></td>
    <td style="background:hsla(0, 0%, 94%, 1);"><span>L094%</span></td>
  </tr>
  <tr>
    <th>silver</th>
    <td style="background:silver;text-align: center;" colspan=6><span>silver</span></td>
  </tr>
  <tr>
    <th>white</th>
    <td style="background:white;text-align: center;" colspan=6><span>white</span></td>
  </tr>
</table>

<table class=tbl>
<caption>teks middle gray</caption>
  <tr>
    <th>hsLa</th>
    <th>≍A001</th>
    <th>≍A.92</th>
    <th>≍A.75</th>
    <th>≍A.50</th>
    <th>≍A.25</th>
    <th>≍A.24</th>
  </tr>
  <tr>
    <th>black</th>
    <td style="color:black;text-align: center;" colspan=6><span>black</span></td>
  </tr>
  <tr>
    <th>Geomean of 60:1</th>
    <td style="color:hsla(0, 0%, 40%, 1);"><span>L040%</span></td>
    <td style="color:hsla(0, 0%, 45%, 1);"><span>L045%</span></td>
    <td style="color:hsla(0, 0%, 55%, 1);"><span>L055%</span></td>
    <td style="color:hsla(0, 0%, 70%, 1);"><span>L070%</span></td>
    <td style="color:hsla(0, 0%, 85%, 1);"><span>L085%</span></td>
    <td style="color:hsla(0, 0%, 93%, 1);"><span>L093%</span></td>
  </tr>
  <tr>
    <th>CIELAB <code>L*a*b*</code></th>
    <td style="color:hsla(0, 0%, 47%, 1);"><span>L047%</span></td>
    <td style="color:hsla(0, 0%, 51%, 1);"><span>L051%</span></td>
    <td style="color:hsla(0, 0%, 60%, 1);"><span>L060%</span></td>
    <td style="color:hsla(0, 0%, 73%, 1);"><span>L073%</span></td>
    <td style="color:hsla(0, 0%, 87%, 1);"><span>L087%</span></td>
    <td style="color:hsla(0, 0%, 87%, 1);"><span>L087%</span></td>
  </tr>
  <tr>
    <th>18% gray card</th>
    <td style="color:hsla(0, 0%, 49%, 1);"><span>L049%</span></td>
    <td style="color:hsla(0, 0%, 53%, 1);"><span>L053%</span></td>
    <td style="color:hsla(0, 0%, 62%, 1);"><span>L062%</span></td>
    <td style="color:hsla(0, 0%, 74%, 1);"><span>L074%</span></td>
    <td style="color:hsla(0, 0%, 87%, 1);"><span>L087%</span></td>
    <td style="color:hsla(0, 0%, 88%, 1);"><span>L088%</span></td>
  </tr>
  <tr>
    <th>sRGB</th>
    <td style="color:hsla(0, 0%, 50%, 1);"><span>L050%</span></td>
    <td style="color:hsla(0, 0%, 54%, 1);"><span>L054%</span></td>
    <td style="color:hsla(0, 0%, 63%, 1);"><span>L063%</span></td>
    <td style="color:hsla(0, 0%, 75%, 1);"><span>L075%</span></td>
    <td style="color:hsla(0, 0%, 87%, 1);"><span>L087%</span></td>
    <td style="color:hsla(0, 0%, 88%, 1);"><span>L088%</span></td>
  </tr>
  <tr>
  <tr>
    <th>gray</th>
    <td style="color:gray;text-align: center;" colspan=6><span>gray</span></td>
  </tr>
    <th>Mac<br/>pre-OS X 10.6</th>
    <td style="color:hsla(0, 0%, 57%, 1);"><span>L057%</span></td>
    <td style="color:hsla(0, 0%, 60%, 1);"><span>L060%</span></td>
    <td style="color:hsla(0, 0%, 67%, 1);"><span>L067%</span></td>
    <td style="color:hsla(0, 0%, 78%, 1);"><span>L078%</span></td>
    <td style="color:hsla(0, 0%, 89%, 1);"><span>L089%</span></td>
    <td style="color:hsla(0, 0%, 90%, 1);"><span>L090%</span></td>
  </tr>
  <tr>
    <th>Absolute whiteness</th>
    <td style="color:hsla(0, 0%, 74%, 1);"><span>L074%</span></td>
    <td style="color:hsla(0, 0%, 76%, 1);"><span>L076%</span></td>
    <td style="color:hsla(0, 0%, 80%, 1);"><span>L080%</span></td>
    <td style="color:hsla(0, 0%, 87%, 1);"><span>L087%</span></td>
    <td style="color:hsla(0, 0%, 93%, 1);"><span>L093%</span></td>
    <td style="color:hsla(0, 0%, 94%, 1);"><span>L094%</span></td>
  </tr>
  <tr>
    <th>silver</th>
    <td style="color:silver;text-align: center;" colspan=6><span>silver</span></td>
  </tr>
  <tr>
    <th>white</th>
    <td style="color:white;text-align: center;" colspan=6><span>white</span></td>
  </tr>
</table>

{{< v >}}
## HSL Chroma {.v}

<table class=tbl-bg>
<caption>latar HSL chroma</caption>
  <tr>
    <th>cr</th>
    <th>o</th>
    <th>u</th>
    <th>a</th>
    <th>e</th>
    <th>i</th>
  </tr>
  <tr>
    <th>black</th>
    <td style="background:hsla(0, 0%, 28%, 1);"><span>cru-k</span></td>
    <td style="background:hsla(0, 0%, 40%, 1);"><span>cro-k</span></td>
    <td style="background:hsla(0, 0%, 50%, 1);"><span>cra-k</span></td>
    <td style="background:hsla(0, 0%, 70%, 1);"><span>cre-k</span></td>
    <td style="background:hsla(0, 0%, 84%, 1);"><span>cri-k</span></td>
  </tr>
  <tr>
    <th>magenta</th>
    <td style="background:hsla(300, 100%, 28%, 1);"><span>cru-m</span></td>
    <td style="background:hsla(300, 100%, 40%, 1);"><span>cro-m</span></td>
    <td style="background:hsla(300, 100%, 50%, 1);"><span>cra-m</span></td>
    <td style="background:hsla(300, 100%, 70%, 1);"><span>cre-m</span></td>
    <td style="background:hsla(300, 100%, 84%, 1);"><span>cri-m</span></td>
  </tr>
  <tr>
    <th>blue</th>
    <td style="background:hsla(200, 100%, 28%, 1);"><span>cru-b</span></td>
    <td style="background:hsla(200, 100%, 40%, 1);"><span>cro-b</span></td>
    <td style="background:hsla(200, 100%, 50%, 1);"><span>cra-b</span></td>
    <td style="background:hsla(200, 100%, 70%, 1);"><span>cre-b</span></td>
    <td style="background:hsla(200, 100%, 84%, 1);"><span>cri-b</span></td>
  </tr>
  <tr>
    <th>cyan</th>
    <td style="background:hsla(180, 100%, 28%, 1);"><span>cru-c</span></td>
    <td style="background:hsla(180, 100%, 40%, 1);"><span>cro-c</span></td>
    <td style="background:hsla(180, 100%, 50%, 1);"><span>cra-c</span></td>
    <td style="background:hsla(180, 100%, 70%, 1);"><span>cre-c</span></td>
    <td style="background:hsla(180, 100%, 84%, 1);"><span>cri-c</span></td>
  </tr>
  <tr>
    <th>green</th>
    <td style="background:hsla(120, 100%, 28%, 1);"><span>cru-c</span></td>
    <td style="background:hsla(120, 100%, 40%, 1);"><span>cro-c</span></td>
    <td style="background:hsla(120, 100%, 50%, 1);"><span>cra-c</span></td>
    <td style="background:hsla(120, 100%, 70%, 1);"><span>cre-c</span></td>
    <td style="background:hsla(120, 100%, 84%, 1);"><span>cri-c</span></td>
  </tr>
  <tr>
    <th>yellow</th>
    <td style="background:hsla(40, 100%, 28%, 1);"><span>cru-y</span></td>
    <td style="background:hsla(40, 100%, 40%, 1);"><span>cro-y</span></td>
    <td style="background:hsla(40, 100%, 50%, 1);"><span>cra-y</span></td>
    <td style="background:hsla(40, 100%, 70%, 1);"><span>cre-y</span></td>
    <td style="background:hsla(40, 100%, 84%, 1);"><span>cri-y</span></td>
  </tr>
  <tr>
    <th>yellow</th>
    <td style="background:hsla(0, 100%, 28%, 1);"><span>cru-y</span></td>
    <td style="background:hsla(0, 100%, 40%, 1);"><span>cro-y</span></td>
    <td style="background:hsla(0, 100%, 50%, 1);"><span>cra-y</span></td>
    <td style="background:hsla(0, 100%, 70%, 1);"><span>cre-y</span></td>
    <td style="background:hsla(0, 100%, 84%, 1);"><span>cri-y</span></td>
  </tr>
</table>

<table class=tbl>
<caption>teks HSL chroma</caption>
  <tr>
    <th>cr</th>
    <th>o</th>
    <th>u</th>
    <th>a</th>
    <th>e</th>
    <th>i</th>
  </tr>
  <tr>
    <th>black</th>
    <td style="color:hsla(0, 0%, 28%, 1);"><span>cru-k</span></td>
    <td style="color:hsla(0, 0%, 40%, 1);"><span>cro-k</span></td>
    <td style="color:hsla(0, 0%, 50%, 1);"><span>cra-k</span></td>
    <td style="color:hsla(0, 0%, 70%, 1);"><span>cre-k</span></td>
    <td style="color:hsla(0, 0%, 84%, 1);"><span>cri-k</span></td>
  </tr>
  <tr>
    <th>magenta</th>
    <td style="color:hsla(300, 100%, 28%, 1);"><span>cru-m</span></td>
    <td style="color:hsla(300, 100%, 40%, 1);"><span>cro-m</span></td>
    <td style="color:hsla(300, 100%, 50%, 1);"><span>cra-m</span></td>
    <td style="color:hsla(300, 100%, 70%, 1);"><span>cre-m</span></td>
    <td style="color:hsla(300, 100%, 84%, 1);"><span>cri-m</span></td>
  </tr>
  <tr>
    <th>blue</th>
    <td style="color:hsla(200, 100%, 28%, 1);"><span>cru-b</span></td>
    <td style="color:hsla(200, 100%, 40%, 1);"><span>cro-b</span></td>
    <td style="color:hsla(200, 100%, 50%, 1);"><span>cra-b</span></td>
    <td style="color:hsla(200, 100%, 70%, 1);"><span>cre-b</span></td>
    <td style="color:hsla(200, 100%, 84%, 1);"><span>cri-b</span></td>
  </tr>
  <tr>
    <th>cyan</th>
    <td style="color:hsla(180, 100%, 28%, 1);"><span>cru-c</span></td>
    <td style="color:hsla(180, 100%, 40%, 1);"><span>cro-c</span></td>
    <td style="color:hsla(180, 100%, 50%, 1);"><span>cra-c</span></td>
    <td style="color:hsla(180, 100%, 70%, 1);"><span>cre-c</span></td>
    <td style="color:hsla(180, 100%, 84%, 1);"><span>cri-c</span></td>
  </tr>
  <tr>
    <th>green</th>
    <td style="color:hsla(120, 100%, 28%, 1);"><span>cru-c</span></td>
    <td style="color:hsla(120, 100%, 40%, 1);"><span>cro-c</span></td>
    <td style="color:hsla(120, 100%, 50%, 1);"><span>cra-c</span></td>
    <td style="color:hsla(120, 100%, 70%, 1);"><span>cre-c</span></td>
    <td style="color:hsla(120, 100%, 84%, 1);"><span>cri-c</span></td>
  </tr>
  <tr>
    <th>yellow</th>
    <td style="color:hsla(40, 100%, 28%, 1);"><span>cru-y</span></td>
    <td style="color:hsla(40, 100%, 40%, 1);"><span>cro-y</span></td>
    <td style="color:hsla(40, 100%, 50%, 1);"><span>cra-y</span></td>
    <td style="color:hsla(40, 100%, 70%, 1);"><span>cre-y</span></td>
    <td style="color:hsla(40, 100%, 84%, 1);"><span>cri-y</span></td>
  </tr>
  <tr>
    <th>yellow</th>
    <td style="color:hsla(0, 100%, 28%, 1);"><span>cru-y</span></td>
    <td style="color:hsla(0, 100%, 40%, 1);"><span>cro-y</span></td>
    <td style="color:hsla(0, 100%, 50%, 1);"><span>cra-y</span></td>
    <td style="color:hsla(0, 100%, 70%, 1);"><span>cre-y</span></td>
    <td style="color:hsla(0, 100%, 84%, 1);"><span>cri-y</span></td>
  </tr>
</table>

{{< v >}}
## HSL Materi {.v}

<table class=tbl-bg>
<caption>latar HSL materi</caption>
  <tr>
    <th>cr</th>
    <th>o</th>
    <th>u</th>
    <th>a</th>
    <th>e</th>
    <th>i</th>
  </tr>
  <tr>
    <th>black</th>
    <td style="background:hsla(0, 0%, 28%, 1);"><span>cru-k</span></td>
    <td style="background:hsla(0, 0%, 40%, 1);"><span>cro-k</span></td>
    <td style="background:hsla(0, 0%, 50%, 1);"><span>cra-k</span></td>
    <td style="background:hsla(0, 0%, 70%, 1);"><span>cre-k</span></td>
    <td style="background:hsla(0, 0%, 84%, 1);"><span>cri-k</span></td>
  </tr>
  <tr>
    <th>magenta</th>
    <td style="background:hsla(300, 100%, 28%, 1);"><span>cru-m</span></td>
    <td style="background:hsla(300, 100%, 40%, 1);"><span>cro-m</span></td>
    <td style="background:hsla(300, 80%, 50%, 1);"><span>cra-m</span></td>
    <td style="background:hsla(300, 80%, 70%, 1);"><span>cre-m</span></td>
    <td style="background:hsla(300, 80%, 84%, 1);"><span>cri-m</span></td>
  </tr>
  <tr>
    <th>blue</th>
    <td style="background:hsla(200, 100%, 28%, 1);"><span>cru-b</span></td>
    <td style="background:hsla(200, 100%, 40%, 1);"><span>cro-b</span></td>
    <td style="background:hsla(200, 100%, 50%, 1);"><span>cra-b</span></td>
    <td style="background:hsla(200, 100%, 70%, 1);"><span>cre-b</span></td>
    <td style="background:hsla(200, 100%, 84%, 1);"><span>cri-b</span></td>
  </tr>
  <tr>
    <th>cyan</th>
    <td style="background:hsla(180, 100%, 28%, 1);"><span>cru-c</span></td>
    <td style="background:hsla(180, 100%, 40%, 1);"><span>cro-c</span></td>
    <td style="background:hsla(180, 70%, 50%, 1);"><span>cra-c</span></td>
    <td style="background:hsla(180, 70%, 70%, 1);"><span>cre-c</span></td>
    <td style="background:hsla(180, 70%, 84%, 1);"><span>cri-c</span></td>
  </tr>
  <tr>
    <th>green</th>
    <td style="background:hsla(120, 100%, 28%, 1);"><span>cru-c</span></td>
    <td style="background:hsla(120, 100%, 40%, 1);"><span>cro-c</span></td>
    <td style="background:hsla(120, 70%, 50%, 1);"><span>cra-c</span></td>
    <td style="background:hsla(120, 70%, 70%, 1);"><span>cre-c</span></td>
    <td style="background:hsla(120, 70%, 84%, 1);"><span>cri-c</span></td>
  </tr>
  <tr>
    <th>yellow</th>
    <td style="background:hsla(40, 100%, 28%, 1);"><span>cru-y</span></td>
    <td style="background:hsla(40, 100%, 40%, 1);"><span>cro-y</span></td>
    <td style="background:hsla(40, 80%, 50%, 1);"><span>cra-y</span></td>
    <td style="background:hsla(40, 80%, 70%, 1);"><span>cre-y</span></td>
    <td style="background:hsla(40, 80%, 84%, 1);"><span>cri-y</span></td>
  </tr>
  <tr>
    <th>yellow</th>
    <td style="background:hsla(0, 100%, 28%, 1);"><span>cru-y</span></td>
    <td style="background:hsla(0, 100%, 40%, 1);"><span>cro-y</span></td>
    <td style="background:hsla(0, 70%, 50%, 1);"><span>cra-y</span></td>
    <td style="background:hsla(0, 70%, 70%, 1);"><span>cre-y</span></td>
    <td style="background:hsla(0, 70%, 84%, 1);"><span>cri-y</span></td>
  </tr>
</table>

<table class=tbl>
<caption>teks HSL materi</caption>
  <tr>
    <th>cr</th>
    <th>o</th>
    <th>u</th>
    <th>a</th>
    <th>e</th>
    <th>i</th>
  </tr>
  <tr>
    <th>black</th>
    <td style="color:hsla(0, 0%, 28%, 1);"><span>cru-k</span></td>
    <td style="color:hsla(0, 0%, 40%, 1);"><span>cro-k</span></td>
    <td style="color:hsla(0, 0%, 50%, 1);"><span>cra-k</span></td>
    <td style="color:hsla(0, 0%, 70%, 1);"><span>cre-k</span></td>
    <td style="color:hsla(0, 0%, 84%, 1);"><span>cri-k</span></td>
  </tr>
  <tr>
    <th>magenta</th>
    <td style="color:hsla(300, 100%, 28%, 1);"><span>cru-m</span></td>
    <td style="color:hsla(300, 100%, 40%, 1);"><span>cro-m</span></td>
    <td style="color:hsla(300, 80%, 50%, 1);"><span>cra-m</span></td>
    <td style="color:hsla(300, 80%, 70%, 1);"><span>cre-m</span></td>
    <td style="color:hsla(300, 80%, 84%, 1);"><span>cri-m</span></td>
  </tr>
  <tr>
    <th>blue</th>
    <td style="color:hsla(200, 100%, 28%, 1);"><span>cru-b</span></td>
    <td style="color:hsla(200, 100%, 40%, 1);"><span>cro-b</span></td>
    <td style="color:hsla(200, 100%, 50%, 1);"><span>cra-b</span></td>
    <td style="color:hsla(200, 100%, 70%, 1);"><span>cre-b</span></td>
    <td style="color:hsla(200, 100%, 84%, 1);"><span>cri-b</span></td>
  </tr>
  <tr>
    <th>cyan</th>
    <td style="color:hsla(180, 100%, 28%, 1);"><span>cru-c</span></td>
    <td style="color:hsla(180, 100%, 40%, 1);"><span>cro-c</span></td>
    <td style="color:hsla(180, 70%, 50%, 1);"><span>cra-c</span></td>
    <td style="color:hsla(180, 70%, 70%, 1);"><span>cre-c</span></td>
    <td style="color:hsla(180, 70%, 84%, 1);"><span>cri-c</span></td>
  </tr>
  <tr>
    <th>green</th>
    <td style="color:hsla(120, 100%, 28%, 1);"><span>cru-c</span></td>
    <td style="color:hsla(120, 100%, 40%, 1);"><span>cro-c</span></td>
    <td style="color:hsla(120, 70%, 50%, 1);"><span>cra-c</span></td>
    <td style="color:hsla(120, 70%, 70%, 1);"><span>cre-c</span></td>
    <td style="color:hsla(120, 70%, 84%, 1);"><span>cri-c</span></td>
  </tr>
  <tr>
    <th>yellow</th>
    <td style="color:hsla(40, 100%, 28%, 1);"><span>cru-y</span></td>
    <td style="color:hsla(40, 100%, 40%, 1);"><span>cro-y</span></td>
    <td style="color:hsla(40, 80%, 50%, 1);"><span>cra-y</span></td>
    <td style="color:hsla(40, 80%, 70%, 1);"><span>cre-y</span></td>
    <td style="color:hsla(40, 80%, 84%, 1);"><span>cri-y</span></td>
  </tr>
  <tr>
    <th>yellow</th>
    <td style="color:hsla(0, 100%, 28%, 1);"><span>cru-y</span></td>
    <td style="color:hsla(0, 100%, 40%, 1);"><span>cro-y</span></td>
    <td style="color:hsla(0, 70%, 50%, 1);"><span>cra-y</span></td>
    <td style="color:hsla(0, 70%, 70%, 1);"><span>cre-y</span></td>
    <td style="color:hsla(0, 70%, 84%, 1);"><span>cri-y</span></td>
  </tr>
</table>

{{< v >}}
## Middle HSL {.v}

<table class=tbl-bg>
<caption>latar middle HSL</caption>
  <tr>
    <th>cr</th>
    <th>o</th>
    <th>u</th>
    <th>a</th>
    <th>e</th>
    <th>i</th>
    <th>ii</th>
  </tr>
  <tr>
    <th>black</th>
    <td style="background:hsla(0, 0%, 24%, 1);"><span>cru-k</span></td>
    <td style="background:hsla(0, 0%, 38%, 1);"><span>cro-k</span></td>
    <td style="background:hsla(0, 0%, 50%, 1);"><span>cra-k</span></td>
    <td style="background:hsla(0, 0%, 70%, 1);"><span>cre-k</span></td>
    <td style="background:hsla(0, 0%, 86%, 1);"><span>cri-k</span></td>
    <td style="background:hsla(0, 0%, 92%, 1);"><span>cri-k</span></td>
  </tr>
  <tr>
    <th>magenta</th>
    <td style="background:hsla(300, 100%, 24%, 1);"><span>cru-m</span></td>
    <td style="background:hsla(300, 100%, 44%, 1);"><span>cro-m</span></td>
    <td style="background:hsla(300, 100%, 64%, 1);"><span>cra-m</span></td>
    <td style="background:hsla(300, 100%, 78%, 1);"><span>cre-m</span></td>
    <td style="background:hsla(300, 100%, 86%, 1);"><span>cre-m</span></td>
    <td style="background:hsla(300, 100%, 92%, 1);"><span>cri-m</span></td>
  </tr>
  <tr>
    <th>blue</th>
    <td style="background:hsla(200, 100%, 24%, 1);"><span>cru-b</span></td>
    <td style="background:hsla(200, 100%, 35%, 1);"><span>cro-b</span></td>
    <td style="background:hsla(200, 100%, 45%, 1);"><span>cra-b</span></td>
    <td style="background:hsla(200, 100%, 68%, 1);"><span>cre-b</span></td>
    <td style="background:hsla(200, 100%, 86%, 1);"><span>cre-b</span></td>
    <td style="background:hsla(200, 100%, 92%, 1);"><span>cri-b</span></td>
  </tr>
  <tr>
    <th>cyan</th>
    <td style="background:hsla(180, 100%, 24%, 1);"><span>cru-c</span></td>
    <td style="background:hsla(180, 100%, 27%, 1);"><span>cro-c</span></td>
    <td style="background:hsla(180, 100%, 32%, 1);"><span>cra-c</span></td>
    <td style="background:hsla(180, 100%, 62%, 1);"><span>cre-c</span></td>
    <td style="background:hsla(180, 100%, 86%, 1);"><span>cre-c</span></td>
    <td style="background:hsla(180, 100%, 92%, 1);"><span>cri-c</span></td>
  </tr>
  <tr>
    <th>green</th>
    <td style="background:hsla(120, 100%, 24%, 1);"><span>cru-c</span></td>
    <td style="background:hsla(120, 100%, 30%, 1);"><span>cro-c</span></td>
    <td style="background:hsla(120, 100%, 34%, 1);"><span>cra-c</span></td>
    <td style="background:hsla(120, 100%, 68%, 1);"><span>cre-c</span></td>
    <td style="background:hsla(120, 100%, 86%, 1);"><span>cre-c</span></td>
    <td style="background:hsla(120, 100%, 92%, 1);"><span>cri-c</span></td>
  </tr>
  <tr>
    <th>yellow</th>
    <td style="background:hsla(40, 100%, 24%, 1);"><span>cru-y</span></td>
    <td style="background:hsla(40, 100%, 30%, 1);"><span>cro-y</span></td>
    <td style="background:hsla(40, 100%, 36%, 1);"><span>cra-y</span></td>
    <td style="background:hsla(40, 100%, 64%, 1);"><span>cre-y</span></td>
    <td style="background:hsla(40, 100%, 86%, 1);"><span>cre-y</span></td>
    <td style="background:hsla(40, 100%, 92%, 1);"><span>cri-y</span></td>
  </tr>
  <tr>
    <th>yellow</th>
    <td style="background:hsla(0, 100%, 24%, 1);"><span>cru-y</span></td>
    <td style="background:hsla(0, 100%, 46%, 1);"><span>cro-y</span></td>
    <td style="background:hsla(0, 100%, 68%, 1);"><span>cra-y</span></td>
    <td style="background:hsla(0, 100%, 82%, 1);"><span>cre-y</span></td>
    <td style="background:hsla(0, 100%, 86%, 1);"><span>cre-y</span></td>
    <td style="background:hsla(0, 100%, 92%, 1);"><span>cri-y</span></td>
  </tr>
</table>

<table class=tbl>
<caption>teks middle HSL</caption>
  <tr>
    <th>cr</th>
    <th>o</th>
    <th>u</th>
    <th>a</th>
    <th>e</th>
    <th>i</th>
    <th>ii</th>
  </tr>
  <tr>
    <th>black</th>
    <td style="color:hsla(0, 0%, 24%, 1);"><span>cru-k</span></td>
    <td style="color:hsla(0, 0%, 38%, 1);"><span>cro-k</span></td>
    <td style="color:hsla(0, 0%, 50%, 1);"><span>cra-k</span></td>
    <td style="color:hsla(0, 0%, 70%, 1);"><span>cre-k</span></td>
    <td style="color:hsla(0, 0%, 86%, 1);"><span>cri-k</span></td>
    <td style="color:hsla(0, 0%, 92%, 1);"><span>cri-k</span></td>
  </tr>
  <tr>
    <th>magenta</th>
    <td style="color:hsla(300, 100%, 24%, 1);"><span>cru-m</span></td>
    <td style="color:hsla(300, 100%, 44%, 1);"><span>cro-m</span></td>
    <td style="color:hsla(300, 100%, 64%, 1);"><span>cra-m</span></td>
    <td style="color:hsla(300, 100%, 78%, 1);"><span>cre-m</span></td>
    <td style="color:hsla(300, 100%, 86%, 1);"><span>cre-m</span></td>
    <td style="color:hsla(300, 100%, 92%, 1);"><span>cri-m</span></td>
  </tr>
  <tr>
    <th>blue</th>
    <td style="color:hsla(200, 100%, 24%, 1);"><span>cru-b</span></td>
    <td style="color:hsla(200, 100%, 35%, 1);"><span>cro-b</span></td>
    <td style="color:hsla(200, 100%, 45%, 1);"><span>cra-b</span></td>
    <td style="color:hsla(200, 100%, 68%, 1);"><span>cre-b</span></td>
    <td style="color:hsla(200, 100%, 86%, 1);"><span>cre-b</span></td>
    <td style="color:hsla(200, 100%, 92%, 1);"><span>cri-b</span></td>
  </tr>
  <tr>
    <th>cyan</th>
    <td style="color:hsla(180, 100%, 24%, 1);"><span>cru-c</span></td>
    <td style="color:hsla(180, 100%, 27%, 1);"><span>cro-c</span></td>
    <td style="color:hsla(180, 100%, 32%, 1);"><span>cra-c</span></td>
    <td style="color:hsla(180, 100%, 62%, 1);"><span>cre-c</span></td>
    <td style="color:hsla(180, 100%, 86%, 1);"><span>cre-c</span></td>
    <td style="color:hsla(180, 100%, 92%, 1);"><span>cri-c</span></td>
  </tr>
  <tr>
    <th>green</th>
    <td style="color:hsla(120, 100%, 24%, 1);"><span>cru-c</span></td>
    <td style="color:hsla(120, 100%, 30%, 1);"><span>cro-c</span></td>
    <td style="color:hsla(120, 100%, 34%, 1);"><span>cra-c</span></td>
    <td style="color:hsla(120, 100%, 68%, 1);"><span>cre-c</span></td>
    <td style="color:hsla(120, 100%, 86%, 1);"><span>cre-c</span></td>
    <td style="color:hsla(120, 100%, 92%, 1);"><span>cri-c</span></td>
  </tr>
  <tr>
    <th>yellow</th>
    <td style="color:hsla(40, 100%, 24%, 1);"><span>cru-y</span></td>
    <td style="color:hsla(40, 100%, 30%, 1);"><span>cro-y</span></td>
    <td style="color:hsla(40, 100%, 36%, 1);"><span>cra-y</span></td>
    <td style="color:hsla(40, 100%, 64%, 1);"><span>cre-y</span></td>
    <td style="color:hsla(40, 100%, 86%, 1);"><span>cre-y</span></td>
    <td style="color:hsla(40, 100%, 92%, 1);"><span>cri-y</span></td>
  </tr>
  <tr>
    <th>yellow</th>
    <td style="color:hsla(0, 100%, 24%, 1);"><span>cru-y</span></td>
    <td style="color:hsla(0, 100%, 46%, 1);"><span>cro-y</span></td>
    <td style="color:hsla(0, 100%, 68%, 1);"><span>cra-y</span></td>
    <td style="color:hsla(0, 100%, 82%, 1);"><span>cre-y</span></td>
    <td style="color:hsla(0, 100%, 86%, 1);"><span>cre-y</span></td>
    <td style="color:hsla(0, 100%, 92%, 1);"><span>cri-y</span></td>
  </tr>
</table>

{{< v >}}