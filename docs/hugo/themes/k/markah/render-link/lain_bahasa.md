---
judul: "Lain Bahasa di Laman yang Sama"
draft: true
---

`<bdi>` atau `<span lang="kode-bahasa">`  

contoh:  
`[You can say that again](?en "That's true, I agree")`  
[You can say that again](?en "That's true, I agree")

### Arabiyah
[العربية](?ar "Bahasa Arab")
### Jerman
[Deutsch](?de "Jerman")
### Inggris
[English](?en "Inggris")
### Perancis
[Français ](?fr "Perancis")
### Ibrani
[עִבְרִית](?he "Ibrani")
### Hindi
[हिन्दी](?hi "hindi")
### Bahasa Indonesia
[Bahasa Indonesia](?id "Indonesian")
### Italia
[Italiano](?it "Italia")
### Jawa
[ꦧꦱꦗꦮ](?jv "Basa Jawa")
### Jepang
[日本語](?ja "Nihongo")
### Korea
[한국어](?ko "Hangugeo")
### Latin
[Latīnum](?la "Latin")
### Belanda
[Dutch](?nl "Nederlands")
### Rusia
[Русский язык](?ru "Bahasa Rusia")
### Tagalog
[ᜏᜒᜃᜅ᜔ ᜆᜄᜎᜓᜄ᜔](?tl "Tagalog")
### Cina
[汉语](?zh-hans "Hànyǔ")
### Mandarin
[漢語](?zh-hant "Hànyǔ")
### lainnya
[abc](?lang "123")