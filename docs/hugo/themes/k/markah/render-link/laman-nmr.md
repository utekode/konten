---
judul: "Nomor Halaman"
deskripsi: "Batas (dan Nomor) Halaman"
---

## Horizon {.v}
Ditulis:  

    ---

Tampil:  

---  

Ditulis:  

    ***
Tampil:  
***

{{< v >}}

## Halaman {.v}

`<hr>` `role="doc-pagebreak"`

Ditulis:  

    [halaman](001)

Tampil:  
[halaman](001)

Ditulis:  

    [hlm.](002)

Tampil:  
[hlm.](002)

Ditulis:  

    [hal.](034)

Tampil:  
[hal.](034)

Ditulis:  

    [laman](234 "berdasarkan naskah sumber cetak")

Tampil:  
[laman](234 "berdasarkan naskah sumber cetak")

{{< v >}}