---
judul: Fungsi
---

### Singkatan atau *Abbreviations* `<abbr>` {.x} 
Ditulis:  

    Pengatakan dilakukan menggunakan [CSS](:abbr "Cascading Style Sheets")  
    [EBI](:singkat "Ejaan Bahasa Indonesia")  
    [PUEBI](:singkatan "Panduan Umum Ejaan Bahasa Indonesia")

Tampil:  
Pengatakan dilakukan menggunakan [CSS](:abbr "Cascading Style Sheets")  
[EBI](:singkat "Ejaan Bahasa Indonesia")  
[PUEBI](:singkatan "Panduan Umum Ejaan Bahasa Indonesia")

{{< x >}}
### Akronim `<acronym>` {.x}
Ditulis:  

    [Posyandu](:akronim "Pos Pelayanan Terpadu")  

Tampil:  
[Posyandu](:akronim "Pos Pelayanan Terpadu")  

{{< x >}}
### Definisi `<dfn>` {.x}
Ditulis:  

    [Definisi](:definisi) kata, frasa, atau kalimat yang mengungkapkan makna, keterangan, atau ciri utama dari orang, benda, proses, atau aktivitas; batasan (arti)

Tampil:  
[Definisi](:definisi) kata, frasa, atau kalimat yang mengungkapkan makna, keterangan, atau ciri utama dari orang, benda, proses, atau aktivitas; batasan (arti)

{{<x>}}
### Kutip `<q>` {.x}
Ditulis:  

    Alcuin bilang [vox populi, vox dei](:kutip$lang;latin "suara rakyat, suara tuhan")

Tampil:  
Alcuin bilang [vox populi, vox dei](:kutip$lang;latin "suara rakyat, suara tuhan")

{{< x >}}
### *Cite* `<cite>` {.x}
Ditulis:  

    Sumber [Wikipedia](https://wikipedia.org ":sumber.berita")

Tampil:  
Sumber [Wikipedia](https://wikipedia.org ":sumber.berita")

Ditulis:  

    Dari [Wiktionary](:cite.hero "Wiktionary")

Tampil:  
Dari [Wiktionary](:cite.hero "Wiktionary")

{{< x >}}