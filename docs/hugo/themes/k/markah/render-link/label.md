---
Judul: "Label"
---

### 1 · Abu-abu
`[apapun](! "judul atau penjelas")`

ini adalah [apapun](! "judul atau penjelas") di dalam kalimat.

### 2 · Biru
`[info](!! "aktif")`

ini adalah [info](!! "aktif") di dalam kalimat.

### 3 · Hijau
`[penting](!!! "waspada")`

ini adalah [penting](!!! "waspada") di dalam kalimat.

### 4 · Kuning
`[info](!!!! "siaga")`

ini adalah [ingat](!!!! "siaga") di dalam kalimat.

### 5 · Merah
`[bahaya](!!!!!dilarang.melarang "awas")`

ini adalah [bahaya](!!!!!dilarang.melarang "awas") di dalam kalimat.

## Label Taut

[DOC](!! "Word document")  
[XLS](!!! "Excel document")  
[PPT](!!!! "Powerpoint document")  
[PDF](!!!!! "PDF document")  

[.md](. "!!MarkDown")  
[.csv](. "!!!Comma-Separated Values")  
[.svg](. "!!!!Scalable Vector Graphics")  
[.epub](. "!!!!!Electronic Publication")  

## Tautan dalam Label [percobaan](!!!! "test")

[mencoba menulis [tautan](.) di dalam label](!! "judul")  

![mencoba menulis [tautan](.) di dalam label](!! "judul")  

[mencoba menulis *footnote* [^1] di dalam admonition](!!! "Judul")  

![mencoba menulis *footnote* [^1] di dalam admonition](!!! "Judul")  

![mencoba menulis [*footnote*][footnote] di dalam admonition](!!! "Judul")  

[footnote]: /aplikasi/