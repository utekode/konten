---
judul: "Params Sosial"  
deskripsi: "Parameter kirim sosial"
---

## `Params`  
+ fbpost  
+ tgpost  
+ tweet  

### Tombol bagi  
pohon *native Share Intent API*  
+ bagi  
  + whatsapp (web only)  
  + instagram (mobile only: file; image intent)  
  + facebook (web sharer)  
  + twitter (tweet intent)  
  + telegram (web sharer)  
  + kirim (mobile only: native mobile intent)  
