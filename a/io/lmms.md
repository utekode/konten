---
judul: "LMMS"
deskripsi: "DAW (Stasiun Kerja Audio Digital) Perangkat lunak pengolah lagu."
os: 
  - Linux
  - Windows
  - MacOS
aplikasi: 
  - Audio
  - Musik
  - DAW
warna: "#13b248"
---