---
judul: "LibreOffice Writer"
deskripsi: "Perangkat lunak pembuatan dokumen dan pengolahan kata."
os: 
  - Linux
  - Windows
  - MacOS
aplikasi: 
  - Dokumen
  - Kantor
  - Teks
warna: "#1287c8"
---