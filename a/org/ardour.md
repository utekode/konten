---
judul: "Ardour"
deskripsi: "DAW (Stasiun Kerja Audio Digital) Perangkat lunak pengolah lagu."
os: 
  - Linux
  - Windows
  - MacOS
aplikasi: 
  - Audio
  - Music
  - DAW
warna: "#fb2020"
---