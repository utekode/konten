---
judul: "Adobe Photoshop"
deskripsi: "Perangkat lunak pengolah grafis dan citra foto."
os: 
  - Windows
  - MacOS
aplikasi: 
  - Foto
  - Grafika
warna: "#00c9ff"
---