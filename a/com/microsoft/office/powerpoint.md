---
judul: "Microsoft Office PowerPoint"
deskripsi: "Perangkat lunak presentasi slide."
os: 
  - Android
  - Windows
  - MacOS
aplikasi: 
  - Dokumen
  - Kantor
  - Presentasi
warna: "#f04e23"
---