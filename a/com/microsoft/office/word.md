---
judul: "Microsoft Office Word"
deskripsi: "Perangkat lunak pembuatan dokumen dan pengolahan kata."
os: 
  - Android
  - Windows
  - MacOS
aplikasi: 
  - Dokumen
  - Kantor
  - Teks
warna: "#0054a6"
---