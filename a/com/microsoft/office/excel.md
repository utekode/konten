---
judul: "Microsoft Office Excel"
deskripsi: "Perangkat lunak pembuatan tabel dan pengolahan data."
os: 
  - Android
  - Windows
  - MacOS
aplikasi: 
  - Dokumen
  - Kantor
  - Tabel
warna: "#008641"
---