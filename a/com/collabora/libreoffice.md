---
judul: "Collabora Office"
deskripsi: "Perangkat lunak pengolah dokumen teks, tabel, presentasi, berbasis LibreOffice."
os: 
  - Android
  - iOS
aplikasi: 
  - Dokumen
  - Kantor
  - Teks
  - Tabel
  - Presentasi
warna: "#504999"
---